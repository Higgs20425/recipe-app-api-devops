terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-s3"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Porject     = var.project
    Owner       = var.project
    ManageBy    = "Terraform"
  }
}

data "aws_region" "current" {}